use aws_config::load_from_env;
use aws_sdk_dynamodb::{model::AttributeValue, Client};
use lambda_runtime::{service_fn, Error as LambdaError, LambdaEvent};
use rand::Rng;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use simple_logger::SimpleLogger;

// write a function to post new information to AWS DynamoDB

#[derive(Deserialize)]
struct Request {
    command: Option<String>,
}

#[derive(Serialize)]
struct Response {
    command: String,
}

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    SimpleLogger::new().with_utc_timestamps().init()?;
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Value>) -> Result<Response, LambdaError> {
    // Check that command is present
    let request: Request = serde_json::from_value(event.payload)?;
    if request.command.is_none() {
        let resp = Response {
            command: "No command provided".to_string(),
        };
        return Ok(resp);
    }

    let mut rng = rand::thread_rng();
    let request = rng.gen_range(0..100000);

    // //converting request to optional string
    // let request = Some(request.to_string());

    let config = load_from_env().await;
    let client = Client::new(&config);

    post_new_info(&client, request).await?;

    // converting response to string
    let request = request.to_string();

    let resp = Response { command: request };
    Ok(resp)
}

async fn post_new_info(client: &Client, id: i64) -> Result<(), LambdaError> {
    let table_name = "ind-4"; // Make sure this matches your DynamoDB table name
    let id_av = AttributeValue::N(id.to_string());

    client
        .put_item()
        .table_name(table_name)
        .item("id", id_av)
        .send()
        .await?;

    Ok(())
}
