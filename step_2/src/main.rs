use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, tracing, Error, LambdaEvent};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct Request {
    command: String,
}

#[derive(Serialize)]
struct Response {
    total_values: String,
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let _command = event.payload.command;
    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);

    let _resp = client.scan().table_name("ind-4").send().await?;

    let mut head_count = 0;
    if let Some(item) = _resp.items {
        head_count = item.iter().count();
    }
    // Prepare the response
    let resp = Response {
        total_values: head_count.to_string(),
    };

    // Return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
